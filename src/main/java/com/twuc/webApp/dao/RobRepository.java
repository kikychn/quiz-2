package com.twuc.webApp.dao;

import com.twuc.webApp.model.Rob;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RobRepository extends JpaRepository<Rob, Long> {
    List<Rob> findAllByOrderById();
}
