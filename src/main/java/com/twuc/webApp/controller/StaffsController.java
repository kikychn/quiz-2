package com.twuc.webApp.controller;

import com.twuc.webApp.dao.RobRepository;
import com.twuc.webApp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class StaffsController {

    @Autowired
    private RobRepository robRepository;

    @PostMapping("/staffs")
    public ResponseEntity createRob(@RequestBody @Valid Name name) throws URISyntaxException {
        Rob rob = robRepository.save(new Rob(name.getFirstName(), name.getLastName()));
        Long saveId = rob.getId();
        return ResponseEntity.created(new URI("/api/staffs/" + saveId)).build();
    }

    @GetMapping("/staffs/{staffId}")
    public ResponseEntity getRob(@PathVariable Long staffId) {
        Optional<Rob> savedRob = robRepository.findById(staffId);
        if (!savedRob.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(savedRob);
    }

    @GetMapping("/staffs")
    public ResponseEntity getAllBob() {
        List<Rob> robs = robRepository.findAllByOrderById();
        return ResponseEntity.ok().body(robs);
    }

    @PutMapping("/staffs/{staffId}/timezone")
    public ResponseEntity putZone(@PathVariable Long staffId, @RequestBody TimeZone timezone) {
        if (!timezone.check()) {
            return ResponseEntity.status(400).build();
        }
        Optional<Rob> savedRob = robRepository.findById(staffId);
        if (!savedRob.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        savedRob.get().setZoneId(timezone.getZoneId());
        robRepository.flush();
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/timezones")
    public ResponseEntity getTimeZoneSet() {
        TimeZone timeZone = new TimeZone();
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(timeZone.getTimeZoneSet());
    }

    @PostMapping("/staffs/{staffId}/reservations")
    public ResponseEntity createReservation(@PathVariable Long staffId,
                                            @RequestBody @Valid ReservationPostJson reservationPostJson) throws URISyntaxException {
//        new Reservation()
        return ResponseEntity.created(new URI("/api/staffs/" + staffId + "/reservations")).build();
    }
}
