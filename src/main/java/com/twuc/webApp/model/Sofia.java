package com.twuc.webApp.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Sofia {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Length(max = 64)
    @Column(nullable = false, length = 64)
    private String firstName;
    @NotNull
    @Length(max = 64)
    @Column(nullable = false, length = 64)
    private String lastName;

    public Sofia() {
    }

    public Sofia(@NotNull @Length(max = 64) String firstName, @NotNull @Length(max = 64) String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
