package com.twuc.webApp.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

public class Name {
    @NotNull
    @Length(max = 64)
    @Column(nullable = false, length = 64)
    private String firstName;
    @NotNull
    @Length(max = 64)
    @Column(nullable = false, length = 64)
    private String lastName;

    public Name() {
    }

    public Name(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
