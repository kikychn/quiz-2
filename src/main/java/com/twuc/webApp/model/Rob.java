package com.twuc.webApp.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZoneId;
import java.util.List;

@Entity
public class Rob {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Length(max = 64)
    @Column(nullable = false, length = 64)
    private String firstName;
    @NotNull
    @Length(max = 64)
    @Column(nullable = false, length = 64)
    private String lastName;
    @Column
    private ZoneId zoneId = null;
    @OneToMany
    private List<Reservation> reservations;

    public Rob() {
    }

    public Rob(@NotNull @Length(max = 64) String firstName, @NotNull @Length(max = 64) String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public void setZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
    }
}
