package com.twuc.webApp.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ReservationPostJson {
    @NotNull
    @Size(max = 128)
    private String userName;
    @NotNull
    @Size(max = 64)
    private String companyName;
    @NotNull
    private ZoneId zoneId;
    @NotNull
    private ZonedDateTime startTime;
    @NotNull
    private String duration;

    public ReservationPostJson() {
    }

    public ReservationPostJson(@NotNull @Size(max = 128) String userName, @NotNull @Size(max = 64) String companyName, @NotNull ZoneId zoneId, @NotNull ZonedDateTime startTime, @NotNull String duration) {
        this.userName = userName;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
