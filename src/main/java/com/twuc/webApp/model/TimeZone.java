package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.zone.ZoneRulesProvider;
import java.util.Set;

@Component
public class TimeZone {
    private ZoneId zoneId;

    public TimeZone() {
    }

    public TimeZone(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    public boolean check() {
        Set<String> timezones = getTimeZoneSet();
        return timezones.contains(this.zoneId.toString());
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public Set<String> getTimeZoneSet() {
        return ZoneRulesProvider.getAvailableZoneIds();
    }
}
