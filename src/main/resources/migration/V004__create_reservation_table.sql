CREATE TABLE IF NOT EXISTS Reservation (
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY ,
    `name` VARCHAR(128) NOT NULL ,
    `companyName` VARCHAR(64) NOT NULL ,
    `zoneId` VARCHAR(100) NOT NULL ,
    `startTime` DATETIME NOT NULL ,
    `duration` INT NOT NULL
)
