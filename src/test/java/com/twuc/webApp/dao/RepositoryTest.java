package com.twuc.webApp.dao;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.model.Name;
import com.twuc.webApp.model.Reservation;
import com.twuc.webApp.model.Rob;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryTest extends ApiTestBase {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private RobRepository robRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @Test
    void should_save_and_get_rob_entity() {
        Rob rob = robRepository.saveAndFlush(new Rob("a", "bb"));
        Long savedId = rob.getId();
        entityManager.clear();


        Rob savedRob = robRepository.findById(savedId).orElseThrow(NoSuchElementException::new);
        assertEquals("a", savedRob.getFirstName());
        assertEquals("bb", savedRob.getLastName());
    }

    @Test
    void should_save_and_get_revervation_entity() {
        Rob rob = robRepository.saveAndFlush(new Rob("a", "bb"));
        ZoneId zoneId = ZoneId.of("Europe/Paris");
        LocalDateTime localDateTime = LocalDateTime.of(2019, Month.AUGUST, 20, 11, 46);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, zoneId);
        ZoneId zoneIdSofia = ZoneId.of("Africa/Nairobi");
        Reservation reservation = reservationRepository.saveAndFlush(
                new Reservation("Sofia", "ThoughtWorks", zoneIdSofia, zonedDateTime, 1, rob)
        );
        Long savedId = reservation.getId();
        entityManager.clear();

        Reservation savedReservation = reservationRepository.findById(savedId).orElseThrow(NoSuchElementException::new);
        assertEquals("Sofia", savedReservation.getUsername());
        assertEquals("ThoughtWorks", savedReservation.getCompanyName());
        assertEquals("Africa/Nairobi", savedReservation.getZoneId().toString());
        assertEquals(zonedDateTime.withZoneSameInstant(ZoneId.of("Europe/Paris")),
                savedReservation.getStartTime().withZoneSameInstant(ZoneId.of("Europe/Paris")));
        assertEquals(1, savedReservation.getDuration());
    }
}