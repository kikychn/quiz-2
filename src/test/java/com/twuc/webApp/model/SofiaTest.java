package com.twuc.webApp.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SofiaTest {

    @Test
    void getPerson() {
        Sofia sofia = new Sofia("a", "bb");
        assertEquals("a", sofia.getFirstName());
        assertEquals("bb", sofia.getLastName());
    }
}