package com.twuc.webApp.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RobTest {

    @Test
    void getPerson() {
        Rob rob = new Rob("a","bb");
        assertEquals("a",rob.getFirstName());
        assertEquals("bb",rob.getLastName());
    }
}