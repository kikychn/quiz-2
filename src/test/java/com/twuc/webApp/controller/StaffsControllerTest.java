package com.twuc.webApp.controller;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.dao.RobRepository;
import com.twuc.webApp.model.Name;
import com.twuc.webApp.model.ReservationPostJson;
import com.twuc.webApp.model.Rob;
import com.twuc.webApp.model.TimeZone;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


class StaffsControllerTest extends ApiTestBase {

    @Autowired
    private RobRepository robRepository;

    @Test
    void createRob() throws Exception {
        Name name = new Name("Rob", "Hall");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(name)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));
        Rob saveRob = robRepository.findById(1L).orElseThrow(NoSuchElementException::new);
        assertEquals("Rob", saveRob.getFirstName());
        assertEquals("Hall", saveRob.getLastName());
    }

    @Test
    void should_return_rob_or_404_when_request_with_id() throws Exception {
        Name name = new Name("Rob", "Hall");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(name)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("firstName").value("Rob"))
                .andExpect(jsonPath("lastName").value("Hall"));
        mockMvc.perform(get("/api/staffs/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_all_robs_or_empty_array_when_request_staffs() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(content().string("[]"));

        Name name = new Name("Rob", "Hall");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(name)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(name)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/2"));

        mockMvc.perform(get("/api/staffs"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].firstName", is("Rob")))
                .andExpect(jsonPath("$[0].lastName", is("Hall")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].firstName", is("Rob")))
                .andExpect(jsonPath("$[1].lastName", is("Hall")));
    }

    @Test
    void should_put_timezone_to_rob() throws Exception {
        Name name = new Name("Rob", "Hall");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(name)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));

        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"zoneId\": \"Chongqing\"" +
                        "}"))
                .andExpect(status().isBadRequest());

        TimeZone rightTimeZone = new TimeZone(ZoneId.of("Asia/Chongqing"));
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("zoneId").isEmpty());
        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(rightTimeZone)))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("zoneId").value("Asia/Chongqing"));
    }

    @Test
    void should_return_timezone_list() throws Exception {
        mockMvc.perform(get("/api/timezones"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(600)));
    }

    @Test
    void should_return_201_and_location_when_request_add_reservation() throws Exception {
        Name name = new Name("Rob", "Hall");
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2019-08-20T11:46:00+03:00");
        ZoneId zoneId = ZoneId.of("Africa/Nairobi");
        ReservationPostJson reservationPostJson = new ReservationPostJson("Sofia", "ThoughtWorks", zoneId, zonedDateTime, "PT1H");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(name)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));

        mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(reservationPostJson)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1/reservations"));
    }

    @Test
    void should_return_400_when_request_add_reservation() throws Exception {
        Name name = new Name("Rob", "Hall");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(name)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2019-08-20T11:46:00+03:00");
        ZoneId zoneId = ZoneId.of("Africa/Nairobi");
        ReservationPostJson reservationPostJson = new ReservationPostJson("Sofia", "ThoughtWorks", zoneId, zonedDateTime, "PT1H");

        reservationPostJson.setCompanyName(null);
        mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(reservationPostJson)))
                .andExpect(status().isBadRequest());

        mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"username\": \"Sofia\",\n" +
                        "  \"companyName\": \"ThoughtWorks\",\n" +
                        "  \"zoneId\": \"Africa/Nairobi\",\n" +
                        "  \"startTime\": \"2019-08-20T11:46:00\",\n" +
                        "  \"duration\": \"PT1H\"\n" +
                        "}"))
                .andExpect(status().isBadRequest());

        reservationPostJson.setCompanyName("ThoughtWorksThoughtWorksThoughtWorksThoughtWorksThoughtWorksThoughtWorksThoughtWorksThoughtWorksThoughtWorks");
        mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(reservationPostJson)))
                .andExpect(status().isBadRequest());
    }
}
