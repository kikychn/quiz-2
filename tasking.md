1. 创建 Person，Rob, Sofia 用户类型，后两者依赖person。
2. 创建 RobRepository 保存 Rob 用户到数据库。
3. 通过api向系统中添加 Rob 用户，/api/staffs。
4. 获取特定 Rob 用户的信息，/api/staffs/{staffId}。
5. 获取所有 Rob 用户的信息，/api/staffs。
6. 为特定 Rob 用户设置时区信息，/api/staffs/{staffId}/timezone。
7. 验证获取特定 Rob 用户的信息时，带有时区信息。
8. 获取时区列表，/api/timezones。
9. 创建 Reservation 预约单类型。
10. 创建 ReservationRepository 保存Rob用户到数据库。
11. Sofia添加预约，/api/staffs/{staffId}/reservations。
12. 获得预约列表，/api/staffs/{staffId}/reservations。
13. 添加预约规则：Rob 用户必须设置了时区。
14. 添加预约规则：提前两天预约。
15. 添加预约规则：预约时间和持续时间不能够和 Rob 已有的预约冲突。
16. 添加预约规则：预约的开始时间必须在 Rob 的工作时间之内。
17. 添加预约规则：预约时长只能是整小时数，而且只能是 1-3 小时。